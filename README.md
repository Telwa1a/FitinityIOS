--- SPECIFICATION FOR FITINITY ---

Fitinity is a smartphone application for iOS and Android - an application that
allows people to create and keep schedule of their own personal training sessions
in a simple and efficient manner. Fitinity includes a built-in comment/rating
system that gives Fitinity users the ability to comment and rate on a scheduled
training - Their own training and other people’s trainings as well.

A training session in Fitinity consists of multiple elements: Time length of
training session(E.g. 1 hour), title of training session, description of training
session and which day the training session occurs. 

It is required to create and register an account in Fitinity in order to utilize
the app. The registered users will be able to create a training session, update
the training session and delete existing training sessions. The users can define
which sessions that are done by checking/unchecking the training sessions in the
application. As mentioned before, the users of Fitinity will be provided with a
comment/rating system. This means that the user can comment and rate on their own
and other people’s training session. This means that you can give yourself and
other Fitinity users feedback and constructive criticism.

The main focus group are anyone at ages 12 and up. The reasons for this particular
focus group is to make sure that the people using the application are mature enough
to handle a comment/rating-based application. Anyone at ages 12 and up should be 
able to use Fininity, no matter sex/gender/background/etc. In general, this application
is aimed at people who wants to keep themselves healthy in a consistent and easy way.

This application utilizes a backend server application in order to allow users of 
Fitinity to view their own and others training sessions in a organized way - For example,
the Fitinity users should be able view trainings organized after different properties:
Time length, rating, number of comments, etc.

