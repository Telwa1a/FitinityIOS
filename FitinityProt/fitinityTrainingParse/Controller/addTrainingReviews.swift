//
//  addTrainingReviews.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import Parse

class addTrainingReviews: UIViewController {
    
    var trainingnamito = ""
    
    var trainingobjektito = ""
    
    @IBOutlet weak var namuplabel: UILabel!
  
    @IBOutlet weak var reviewtext: UITextView!
    
    @IBOutlet weak var reviewtitle: UITextField!
    
    @IBOutlet var reviewView: UIView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        namuplabel.text = trainingnamito
        
        //Set title of Navigation Bar
        self.navigationItem.title = "Add Review To Training"
        
        reviewtext.delegate = self
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch? = touches.first
        
        if (touch?.view == reviewView) {
            
            if (reviewtitle.isEditing)
            { reviewtitle.endEditing(true)}
            
            reviewtext.endEditing(true)
            
            if (reviewtext.text == "")
            { reviewtext.text = "Text" }
            
            if (reviewtitle.text == "")
            { reviewtitle.text = "Title" }
            
        }
        
    }
    
    func displayAlerts(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
       
            if (title == "Success") {
                
                self.navigationController?.popViewController(animated: true)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //Pressing the title field
    @IBAction func pressingTitle(_ sender: Any) {
        
        if (reviewtitle.text == "Title")
        { reviewtitle.text = "" }
        
    }
    
    @IBAction func doneBtn(_ sender: Any) {
        
        if(reviewtext.text.count > 250){
            let alert = UIAlertController(title: "Text too long", message: "The text should not be longer than 250 characters.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            if(reviewtext.text.isEmpty || reviewtitle.text!.isEmpty){
                
                let alert = UIAlertController(title: "Empty field", message: "Please fill all the fields.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }else{
                
                let review = PFObject(className:"Review")
                
                let userId = PFUser.current()?.objectId;
                
                let userName = PFUser.current()?.username;
                
                review["userid"] = userId
                
                review["whoposted"] = userName
                
                review["reviewTitle"] = reviewtitle.text
                
                review["reviewText"] = reviewtext.text
                
                review["trainingname"] = namuplabel.text
                
                review["trainingObjecId"] = trainingobjektito
                
                let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                
                //specify where we want the acticityIndicator to appear
                
                activityIndicator.center = self.view.center
                
                //when it is done we want to hide it activityIndicator
                
                activityIndicator.hidesWhenStopped = true
                
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
                
                //to add out acitivtyIndicator to the view
                view.addSubview(activityIndicator)
                
                //start animating our controller so it spinns around
                activityIndicator.startAnimating()
                
                //to stop the hole app stop the user to interact with our app
                UIApplication.shared.beginIgnoringInteractionEvents()
                
                review.saveInBackground {
                    
                    (success: Bool, error: Error?) in
                    activityIndicator.stopAnimating()
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    if (success) {
                        
                        self.displayAlerts(title: "Success", message: "Your Review has been posted successfully!")
                        
                        self.reviewtext.text = ""
                        
                        self.reviewtitle.text = ""
                        
                    } else {
                        // There was a problem, check error.description
                        self.displayAlerts(title: "Error", message: "Review wasn't posted - Check your internet connection.")
                        
                    }
                }
            }
        }
    }
}

//Removing placeholder from textview when pressed
extension addTrainingReviews: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if(textView.text=="Text"){
            
            textView.text = ""
        }
    }
}
