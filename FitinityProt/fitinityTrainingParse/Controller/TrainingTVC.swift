//
//  TrainingTVC.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import Parse

class TrainingTVC: UITableViewController {
    
    //Models From Training, Location, Username
     var fetchTraining = [Training]()
    
     var fetchLocation = [Location]()
    
     var fetchUserName = [Username]()
   
    @IBAction func gotoAccounted(_ sender: Any) {
       
        performSegue(withIdentifier: "showAccountTraining", sender: self)
        
    }
    
    @IBAction func goToHistoryReview(_ sender: Any) {
         performSegue(withIdentifier: "showReviewMotivate", sender: self)
    }
    
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: CGFloat(0)/255.0,green: CGFloat(128)/255.0, blue: CGFloat(255)/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    func retriveTraining(){
        //hämta inloggad användare
        let query = PFUser.query()
        
        query?.whereKey("username", notEqualTo: PFUser.current()?.username)
        
        //nu ska vi lägga alla andra användaren i users arrayen
        query?.findObjectsInBackground(block: { (objects, error) in
            
            if let users = objects
            {
                for object in users
                {
                    if let user = object as? PFUser {
                        // self.fetchUserName.append(Username(userName: [user.!] ))
                        // self.fetchUserName.append(Username(userName: [user.ob]))
                        
                    }
                }
            }
            
            //vi vill hämta träningarna uppsatta i på databasen
            let trainingQuery = PFQuery(className: "Training")
            //  print(trainingQuery)
            trainingQuery.findObjectsInBackground(block: { (objects, error) in
                if let trainings = objects
                {
                    
                    self.fetchTraining.removeAll()
                    self.fetchLocation.removeAll()
                    
                    for training in trainings
                    {
                        self.fetchTraining.append(Training(trainingNamei: training["trainingName"] as! String, trainingDayi: training["trainingDay"] as! String, trainingDesci: training["trainingDesc"] as! String, trainingTimei: training["trainingTime"] as! String, objectIdi: String(describing: training.value(forKey: "objectId")!)))
                        
                        if training["latidude"]  != nil && training["longitude"]  != nil {
                            
                            self.fetchLocation.append(Location(latidude: training["latidude"] as! Double, longitude: training["longitude"] as! Double))
                            
                        }
                    }
                }
                
                self.tableView.reloadData()
                
            })
            
            
            
        })
    }
   
    override func viewDidAppear(_ animated: Bool) {
       retriveTraining()
    }
    
    
    @IBAction func addTraining(_ sender: Any) {
        
        performSegue(withIdentifier: "addTrainingSegue", sender: self)
        
    }
    
    @IBAction func logOut(_ sender: Any){
       
        PFUser.logOut();
       
        performSegue(withIdentifier: "LoginView", sender: self);
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return fetchTraining.count
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trainingCell", for: indexPath) as! TrainingTVCell
        
        cell.trainingName.text = fetchTraining[indexPath.row].trainingName
        
        cell.trainingDuration.text = fetchTraining[indexPath.row].trainingTime
        
        cell.trainingDay.text = fetchTraining[indexPath.row].trainingDay
        
        if(indexPath.row % 2 == 1){
            
        cell.contentView.backgroundColor = UIColor(red: CGFloat(247)/255.0,green: CGFloat(247)/255.0, blue: CGFloat(247)/255.0, alpha: 1.0)
          
        }
      
        return cell
    }
 
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 110.0
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowTrainingDetailsVC"{
            
            if let indexPath = tableView.indexPathForSelectedRow
            {
                let destinationVC = segue.destination as! trainingDetailsVC
                
                destinationVC.traningname = fetchTraining[indexPath.row].trainingName
                
                destinationVC.traningdesc = fetchTraining[indexPath.row].trainingDesc
                
                destinationVC.traningtime = fetchTraining[indexPath.row].trainingTime
                
                destinationVC.traningday = fetchTraining[indexPath.row].trainingDay
                
                destinationVC.objectid = fetchTraining[indexPath.row].objectId
                
                destinationVC.lat = fetchLocation[indexPath.row].lat
                
                destinationVC.long = fetchLocation[indexPath.row].long
            }
        }
    }
}




