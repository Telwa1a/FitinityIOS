//
//  GetTrainingReviews.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import Parse

class GetTrainingReviews: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var getObjectId = ""
    
    var getTrainingName = ""
   
    var fetchReview = [Review]()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        self.title = "Reviews for \(getTrainingName)"
        
        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.estimatedRowHeight = 140
        
        getReviews()
        
    }
    
    func getReviews(){
        
        let query = PFQuery(className: "Review")
        
        query.whereKey("trainingObjecId", equalTo: getObjectId)
        
        query.findObjectsInBackground { (objects, error) in
            
            if let reviews = objects {
                
                for review in reviews {
                    
                    self.fetchReview.append(Review(reviewTitle: review["reviewTitle"] as! String, reviewText: review["reviewText"] as! String, usernameReview: review["whoposted"] as! String))
                    
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150.0
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchReview.count
        
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewTVC
        
      cell.reviewTitle.text = fetchReview[indexPath.row].reviewtitle
        
      cell.reviewText.text = fetchReview[indexPath.row].reviewtext
        
      return cell
        
    }
    
}


