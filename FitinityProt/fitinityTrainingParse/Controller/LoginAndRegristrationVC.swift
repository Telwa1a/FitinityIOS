//
//  LoginAndRegristrationVC.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import Parse

class LoginAndRegristrationVC: UIViewController {
    
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var errormessage: UILabel!
    @IBOutlet weak var loginOrSignup: UIButton!
    
    var signupMode: Bool!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        errormessage.isHidden = true

        //Make the view buttons rounded in the corners
        loginOrSignup.layer.cornerRadius = 10
        
        loginOrSignup.layer.cornerRadius = 10
        
        //Scale text in the buttons
        loginOrSignup.titleLabel?.adjustsFontSizeToFitWidth = true
        
        loginOrSignup.titleLabel?.adjustsFontSizeToFitWidth = true
        
        // Setup view according to mode (signup or login)
        if signupMode{
            
            loginOrSignup.setTitle("Register", for: [])
            
            confirmPassword.isHidden = false
            
            confirmPassword.isEnabled = true
            
            confirmPasswordHeight.constant = CGFloat.init(30)
            
        }else{
            
            loginOrSignup.setTitle("Login", for: [])
            
            confirmPassword.isHidden = true
            
            confirmPassword.isEnabled = false
            
            confirmPasswordHeight.constant = CGFloat.init(0)
        }
    }
    //function to display alerts
    func displayAlerts(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func loginOrSignupTapped(_ sender: UIButton) {
        //this is the login action here we fill register an user
        if email.text == "" || password.text == ""
        {
            //alert
            errormessage.isHidden = false
            errormessage.text = "Please add an email and an Password"
                  }
        else
        {
            //to create a spinner
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            //specify where we want the acticityIndicator to appear
            activityIndicator.center = self.view.center
            //when it is done we want to hide it activityIndicator
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            //to add out acitivtyIndicator to the view
            view.addSubview(activityIndicator)
            //start animating our controller so it spinns around
            activityIndicator.startAnimating()
            //stop the user to interact with our app
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            if signupMode
            {
                //signupMode
                if(password.text==confirmPassword.text){
                    
                    let user = PFUser()
                    
                    user.username = email.text
                    
                    user.password = password.text
                    
                    user.signUpInBackground(block: { (success, error) in
                        
                    activityIndicator.stopAnimating()
                    //let user interact with the app again
                    UIApplication.shared.endIgnoringInteractionEvents()
                        
                        if let error = error {
                            
                            self.displayAlerts(title: "Error", message: error.localizedDescription)
                            
                        }
                        else
                        {
                            print("Sign Up Success")
                            //perform a segue
                            self.performSegue(withIdentifier: "showTrainingsegue", sender: self)
                        }
                    })
                }else{
                    activityIndicator.stopAnimating()
                    //let user interact with the app again
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    let alert = UIAlertController(title: "Password doesn't match", message: "The password confirmation is incorrect.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                //loginMode
                if let email = email.text{
                    
                    if let password = password.text{
                        
                        PFUser.logInWithUsername(inBackground: email, password: password, block: { (user, error) in
                            
                            activityIndicator.stopAnimating()
                            //let user interact with the app again
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            if user != nil {
                                
                                print("Login Success")
                                
                                self.performSegue(withIdentifier: "showTrainingsegue", sender: self)
                                
                            }
                            else
                            {
                                var errorText = "Error Unknown"
                                
                                if let error = error {
                                    
                                    errorText = error.localizedDescription
                                    
                                }
                                self.errormessage.isHidden = false
                                self.errormessage.text = errorText
                                self.performSegue(withIdentifier: "goToLogin", sender: self)
                               // self.displayAlerts(title: "Could not log you in", message: errorText)
                                
                               

                            }
                        })
                        
                    }
                }
            }
            
        }
    }
}
