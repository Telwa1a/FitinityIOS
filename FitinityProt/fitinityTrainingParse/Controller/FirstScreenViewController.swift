//
//  FirstScreenViewController.swift
//  fitinityTrainingParse
//
//  Created by André David Izquierdo Hernández on 2017-11-15.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class FirstScreenViewController: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier=="goToLogin"){
            
            let destinationVC = segue.destination as! LoginAndRegristrationVC
            
            destinationVC.signupMode = false
    
        }
        if(segue.identifier=="goToRegister"){
            
            let destinationVC = segue.destination as! LoginAndRegristrationVC
            
            destinationVC.signupMode = true
            
        }
    }
}
