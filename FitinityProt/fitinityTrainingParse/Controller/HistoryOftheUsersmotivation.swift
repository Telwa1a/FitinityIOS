//
//  HistoryOftheUsersmotivation.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-12-11.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import Parse

class HistoryOftheUsersmotivation: UITableViewController {

     
     var fetchReview = [Review]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      getReviews()
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return fetchReview.count
    }

    func getReviews(){
        
        let query = PFQuery(className: "Review")
        
        query.whereKey("userid", equalTo: PFUser.current()?.objectId )
        
        query.findObjectsInBackground { (objects, error) in
            
            if let reviews = objects {
                
            
                
                for review in reviews {
                    
                  
                    self.fetchReview.append(Review(reviewTitle: review["reviewTitle"] as! String, reviewText: review["reviewText"] as! String, usernameReview: review["whoposted"] as! String))
                    
                 
                    
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
 
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }

  
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyReview", for: indexPath) as! historyreview
        
        cell.userName.text = fetchReview[indexPath.row].usernameReview
        cell.reviewtitle.text = fetchReview[indexPath.row].reviewtitle
        
        
        cell.reviewtext.text = fetchReview[indexPath.row].reviewtext
        return cell
        
    }

}
