//
//  AddTrainingVC.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import Parse

class AddTrainingVC: UIViewController, CLLocationManagerDelegate {

    var locationManager = CLLocationManager()
    
    var lat = Double()
    
    var long = Double()
    
    @IBOutlet var trainingView: UIView!
    
    @IBOutlet weak var trainingName: UITextField!
    
    @IBOutlet weak var trainingDesc: UITextField!
    
    @IBOutlet weak var trainingTime: UITextField!
    
    @IBOutlet weak var trainingDay: UITextField!
    
    var maxNameChars = Int(40)
    
    var maxDescChars = Int(135)
    
    var maxTimeChars = Int(35)
    
    var maxDayChars = Int(15)
    
    func showAlert(title: String?, message: String?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            if (title == "Success") {
                
                self.navigationController?.popViewController(animated: true)
                
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation: CLLocation = locations[0]
        
        lat = userLocation.coordinate.latitude;
        
        long = userLocation.coordinate.longitude;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        var touch: UITouch? = touches.first
        
        if (touch?.view == trainingView) {
            
            //Check if any of the fields are being edited
            if (trainingName.isEditing)
            { trainingName.endEditing(true) }
            
            if (trainingDesc.isEditing)
            { trainingDesc.endEditing(true) }
            
            if (trainingTime.isEditing)
            { trainingTime.endEditing(true) }
            
            if (trainingDay.isEditing)
            { trainingDay.endEditing(true) }
            
            if (trainingName.text == "")
            { trainingName.text = "Training Name" }
            
            if (trainingDesc.text == "")
            { trainingDesc.text = "Training Description" }
            
            if (trainingTime.text == "")
            { trainingTime.text = "Training Time" }
            
            if (trainingDay.text == "")
            { trainingDay.text = "Training Day" }
            
        }
        
    }
    
    //Pressing the training name field
    @IBAction func pressingName(_ sender: Any) {
        
        if (trainingName.text == "Training Name")
        { trainingName.text = "" }
        
    }
    
    //Pressing the training desc. field
    @IBAction func pressingDesc(_ sender: Any) {
        
        if (trainingDesc.text == "Training Description")
        { trainingDesc.text = "" }
        
    }
    
    //Pressing the training time field
    @IBAction func pressingTime(_ sender: Any) {
        
        if (trainingTime.text == "Training Time")
        { trainingTime.text = "" }
        
    }
    
    //Pressing the training day field
    @IBAction func pressingDay(_ sender: Any) {
        
        if (trainingDay.text == "Training Day")
        { trainingDay.text = "" }
        
    }
    
    func addTraining(){
        
        if(trainingName.text!.count > maxNameChars){
            
            let alert = UIAlertController(title: "Training Name too long", message: "The name of the training should not be longer than " + String(maxNameChars) + " characters.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if (trainingDesc.text!.count > maxDescChars){
            
            let alert = UIAlertController(title: "Training Description too long", message: "The description of the training should not be longer than " + String(maxDescChars) + " characters.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if (trainingDay.text!.count > maxDayChars){
            
            let alert = UIAlertController(title: "Training Day too long", message: "The name of the day for the training should not be longer than " + String(maxDayChars) + " characters.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else if (trainingTime.text!.count > maxTimeChars){
            
            let alert = UIAlertController(title: "Training Time too long", message: "The text of the training time should not be longer than " + String(maxTimeChars) + " characters.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            let training = PFObject(className: "Training")
            
            //send trainingName , desc, time ,day, the userid
            if trainingTime.text != "" && trainingDesc.text != "" && trainingName.text != "" && trainingDay.text != ""{
                
                training["trainingName"] = trainingName.text
                
                training["trainingDesc"] = trainingDesc.text
                
                training["trainingTime"] = trainingTime.text
                
                training["trainingDay"] = trainingDay.text
                
                if lat != 0.0 && long != 0.0 {
                    training["latidude"] = lat
                    training["longitude"] = long
                }
                //send the userID
                training["userid"] = PFUser.current()?.objectId
                
                let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                
                //specify where we want the acticityIndicator to appear
                
                activityIndicator.center = self.view.center
                
                //when it is done we want to hide it activityIndicator
                
                activityIndicator.hidesWhenStopped = true
                
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
                
                //to add out acitivtyIndicator to the view
                view.addSubview(activityIndicator)
                
                //start animating our controller so it spinns around
                activityIndicator.startAnimating()
                
                //to stop the hole app stop the user to interact with our app
                UIApplication.shared.beginIgnoringInteractionEvents()
                
                training.saveInBackground { (success, error) in
                    
                    activityIndicator.stopAnimating()
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    if(success)
                    {
                        self.showAlert(title: "Success", message: "Your training has been posted successfully!")
                        
                        self.trainingName.text = ""
                        
                        self.trainingDesc.text = ""
                        
                        self.trainingTime.text = ""
                        
                        self.trainingDay.text = ""
                        
                    }
                    else
                    {
                        self.showAlert(title: "Error", message: "Training wasn't posted - Please check your internet connection.")
                    }
                }
            }
                
            else{
                showAlert(title: "Error", message: "Cannot post empty trainings.")
            }
        }
    }
    //The user presses the button for adding a new training
    @IBAction func sendItAlltoParse(_ sender: Any) {
        
        addTraining()
        
    }
  
}
