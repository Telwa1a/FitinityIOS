//
//  UserTrainingTVController.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import Parse

class UserTrainingTVController: UITableViewController {
    
    var fetchTraining = [Training]()
    
    var userName = [Username]()

    @IBOutlet weak var accountName: UILabel!
    
    @IBAction func showTraining(_ sender: Any) {
    
        performSegue(withIdentifier: "goToTraining", sender: self)
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
      
        let query = PFUser.query()
        
        query?.whereKey("username", equalTo: PFUser.current()?.username)
        
        query?.findObjectsInBackground(block: { (objects, error) in
            if let users = objects {
                
                for object in users {
                  
                    self.userName.append(Username(userName: object["username"] as! String, objectId: String(describing: object.value(forKey: "objectId")!)))

                }
            }
            
            let trainingQuery = PFQuery(className: "Training")
            
            trainingQuery.whereKey("userid", equalTo: PFUser.current()?.objectId)
            
            trainingQuery.findObjectsInBackground(block: { (objects, error) in
                
                if let trainings = objects
                {
                    
                    self.fetchTraining.removeAll()
                    
                    for training in trainings{
                        
                       self.fetchTraining.append(Training(trainingNamei: training["trainingName"] as! String, trainingDayi: training["trainingDay"] as! String, trainingDesci: training["trainingDesc"] as! String, trainingTimei: training["trainingTime"] as! String, objectIdi: String(describing: training.value(forKey: "objectId")!)))
                    
                    }
                
                }
                
                self.tableView.reloadData()
                
            })
            
        })
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.fetchTraining.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        accountName.text = userName[0].username
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTrainingCell", for: indexPath) as! UsertrainingTVCell

        cell.trainingName.text = fetchTraining[indexPath.row].trainingName
        
        cell.trainingTime.text = fetchTraining[indexPath.row].trainingTime
        
        cell.trainingDay.text = fetchTraining[indexPath.row].trainingDay
        
        if(indexPath.row % 2 == 1){
            
            cell.contentView.backgroundColor = UIColor(red: CGFloat(247)/255.0,green: CGFloat(247)/255.0, blue: CGFloat(247)/255.0, alpha: 1.0)
            
        }

        return cell
        
    }
 
   
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 60.0
    
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
    }
    
    //TO REMOVE THE DATA (an item in to do list and reloads and sets the list)
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete{
 
            let trainingQuery = PFQuery(className: "Training")
            
            trainingQuery.getObjectInBackground(withId: fetchTraining[indexPath.row].objectId) { (object, error) in
                
                object!.deleteInBackground()
                
            }
            
          //  let reviewQuery = PFQuery(className: "Review")
            
           // reviewQuery.whereKey("trainingObjecId", equalTo: fetchTraining[indexPath.row].objectId)
            
          //  reviewQuery
            
           
            
           
            
            
            
            fetchTraining.remove(at: indexPath.row)
          
            tableView.reloadData()
            
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?
    {
        return "Done"
    }
    
}
