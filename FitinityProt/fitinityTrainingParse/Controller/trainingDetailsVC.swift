//
//  trainingDetailsVC.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import Parse
import MapKit
import CoreLocation

class trainingDetailsVC: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var traningname = ""
    
    var traningdesc = ""
    
    var traningtime = ""
    
    var traningday = ""
    
    var objectid = ""
    
    @IBOutlet weak var addReview: UIButton!
    
    @IBOutlet weak var getReview: UIButton!
    
    var lat = Double()
    
    var long = Double()
  
    @IBOutlet weak var trainingMapView: MKMapView!
    
    @IBOutlet weak var uptrainingname: UILabel!
    
    @IBOutlet weak var descriptiontraining: UILabel!
    
    @IBOutlet weak var trainingbime: UILabel!
    
     @IBOutlet weak var trainingdayin: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        uptrainingname.text = traningname
        
        descriptiontraining.text = traningdesc
        
        trainingbime.text = traningtime
        
        trainingdayin.text = traningday
        
        addReview.layer.cornerRadius = 10
        
        getReview.layer.cornerRadius = 10
        
        let latDelta:CLLocationDegrees = 0.01
        
        let lonDelta: CLLocationDegrees = 0.01
        
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let region = MKCoordinateRegion(center: location, span: span)
        
        self.trainingMapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        
        annotation.title = "Let the training start!"
        
        annotation.subtitle = "Common Fight!"
        
        annotation.coordinate = location
        
        trainingMapView.addAnnotation(annotation)

    }

    @IBAction func backBtn(_ sender: Any) {
        
          _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func getReviews(_ sender: Any) {
        
      performSegue(withIdentifier: "showTrainingReviewsSegue", sender: self)
    
    }

    @IBAction func addReviews(_ sender: Any) {
     
         performSegue(withIdentifier: "addTrainingReviewsSegue", sender: self)
        
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //2 segue one the review and 1 to add review
        if segue.identifier == "showTrainingReviewsSegue"{
          
            let destinationVC = segue.destination as! GetTrainingReviews
            
            destinationVC.getObjectId = objectid
            
            destinationVC.getTrainingName = traningname
            
        }else if segue.identifier == "addTrainingReviewsSegue" {
            
            let destinationViewController = segue.destination as! addTrainingReviews
            
            destinationViewController.trainingnamito = traningname
            
            destinationViewController.trainingobjektito = objectid
        }
    }
    
    
}
