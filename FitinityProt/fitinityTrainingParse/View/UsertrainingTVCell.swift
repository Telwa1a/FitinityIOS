//
//  UsertrainingTVCell.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import Parse


class UsertrainingTVCell: UITableViewCell {
   
    var holetrainingId = ""
    
    @IBOutlet weak var trainingTime: UILabel!
    
    @IBOutlet weak var trainingName: UILabel!
    
    @IBOutlet weak var trainingDay: UILabel!
    
   
   
    
}
