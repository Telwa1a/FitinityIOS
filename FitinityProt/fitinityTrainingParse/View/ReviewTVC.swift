//
//  ReviewTVC.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class ReviewTVC: UITableViewCell {

    @IBOutlet weak var reviewText: UILabel!
    
    @IBOutlet weak var reviewTitle: UILabel!
    
}

