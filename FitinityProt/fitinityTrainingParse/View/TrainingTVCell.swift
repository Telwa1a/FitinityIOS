//
//  TrainingTVCell.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-10-28.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class TrainingTVCell: UITableViewCell {

    @IBOutlet weak var trainingName: UILabel!
    
    @IBOutlet weak var trainingDuration: UILabel!
    
    @IBOutlet weak var trainingDay: UILabel!
    
}
