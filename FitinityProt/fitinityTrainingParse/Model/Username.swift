//
//  Username.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-11-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import Foundation

class Username {
    
    private var _username: String
    
    private var _objectId: String
    
    var username: String {
        
        return _username
        
    }
    
    var objectid: String {
        
        return _objectId
        
    }
    
    init(userName: String, objectId: String ) {
        
        self._username = userName
        
        self._objectId = objectId
        
    }
    
}
