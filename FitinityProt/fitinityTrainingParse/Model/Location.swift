//
//  Location.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-11-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import Foundation

class Location {
    
    private var _lat: Double
    
    private var _long: Double
    
    var lat: Double {
        
        return _lat
        
    }
    
    var long: Double {
        
        return _long
        
    }
    
    init(latidude: Double, longitude: Double ) {
        
        self._lat = latidude
        
        self._long = longitude
        
    }
    
}
