
class Training{
    
    private var _trainingName:String
    
    private var _trainingTime: String
    
    private var _trainingDay: String
    
    private var _trainingDesc: String
    
    private var _objectid: String
    
    var trainingName: String{
        
        return _trainingName
        
    }
    
    var trainingTime: String{
        
        return _trainingTime
        
    }
    
    var trainingDay: String{
        
        return _trainingDay
        
    }
    
    var trainingDesc: String{
        
        return _trainingDesc
        
    }
    
    var objectId:String{
        
        return _objectid
        
    }
    
    init(trainingNamei:String,trainingDayi:String,trainingDesci:String,trainingTimei:String,objectIdi:String) {

        self._trainingName = trainingNamei
        
        self._trainingDay = trainingDayi
        
        self._trainingDesc = trainingDesci
        
        self._trainingTime = trainingTimei
        
        self._objectid = objectIdi
    
    }
    
}
