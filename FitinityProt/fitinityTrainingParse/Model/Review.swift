//
//  Review.swift
//  fitinityTrainingParse
//
//  Created by Christofer Berrette on 2017-11-29.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import Foundation

class Review{
    
    private var _reviewtitle:String
    
    private var _reviewtext: String
    
    private var _usernameReview: String

    var reviewtitle: String{
        
        return _reviewtitle
        
    }
    
    var usernameReview: String{
        return _usernameReview
    }
    
    var reviewtext: String{
        
        return _reviewtext
        
    }
    
    init(reviewTitle:String,reviewText:String, usernameReview: String) {
        
        self._reviewtitle = reviewTitle
        
        self._reviewtext = reviewText
        
        self._usernameReview = usernameReview
        
    }
    
}
