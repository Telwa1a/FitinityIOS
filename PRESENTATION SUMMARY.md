--- PRESENTATION SUMMARY FOR FITINITY ---

Obesity is a common problem in today’s society - The problem 
might lie in how there aren’t any simple and efficient tools
for keeping yourself motivated on training yourself. Well,
Fitinity is the answer to that problem. Fitinity is a sport
planning application that allows users to plan out their own 
trainings in detail. As an example, this application can be 
used in order to motivate competition between yourself and 
other familiars as a way to motivate yourself for the trainings
created on Fitinity.

The main screen contains all the essentials for the Finitity 
users. Here, the user can add/delete trainings, see all their 
own or other people’s trainings, tap and take a closer look at 
the and finally choose to log out of their application. The 
user can also sort through their own or other people’s trainings 
by popularity, ratings, name, etc.

When the user views a training by tapping on it on the main screen, 
the user is redirected to the training screen. Here, the viewer 
can see the details of the chosen training like time and description 
as well as create/view reviews for the training. There’s also a map 
view of the user’s current location(If the user allows the application 
to show the user’s current location). For notice: Whenever a user 
creates/modifies a training, the user will be transferred to a 
seperate screen for creating the training. There are also seperate 
screens for creating or viewing reviews for a training.

The user is shown the login screen when opening up the application 
for the first time on their device. Here, the user can login or 
choose to register an account, whereas the user will be transferred 
to a seperate screen for registering an account. The user must be 
logged in to the Fitinity server in order to utilize the 
functionalities of the application.

So why should you invest in Fitinity? The answer is simple - Low 
development costs, possibility for monetary gain and the esteem 
of fighting against a persistent problem in the modern society.

The application is profitable through the usage of ad revenue - 
E.g. usage of regex analyzer that analyzes the content/descriptions 
of the trainings created by the users and thereafter places out in-app 
advertisement in the application according to most commonly used 
sport-related terms/words by each separate Fitinity user.

The total development cost for Fitinity(Not accounting for continued 
development of the application after a 1.0 functional version has 
been delivered) is estimated to be 90 000 kr: Three developers are 
working on developing the application during five weeks, 8 hours for 
each workday in a week. The hourly pay is 150 kr for each developer. 
40 hours * 5 weeks * 125 kr/developer * 3 developers = 90 000 kr. 
There are no calculated costs for materials/locals since it’s 
developed basement-style(Utilizing locals of familiars for free) 
and the necessary materials are already supplied by the developers 
themselves.

Presumably it’ll take as long time to convert the app as it takes 
to develop the app for iOS, give or take a week for development 
differences between iOS and Android.The Fitinity application is 
simultaneously developed for iOS and Android and the visual layout 
and user interface doesn’t differ between the two versions of the 
application. The coding aspect differs between the two different 
operating systems but the deadline as well as the development 
requirements for a functional version of the Finitinity application 
are essentially identical between the iOS and Android versions of 
the Fitinity application.


